package cloudran;

public class MultiplexingGainObject implements Comparable<MultiplexingGainObject>{
    String id;
    double multiplexingGain;

    public MultiplexingGainObject(String id, double multiplexingGain) {
        this.id = id;
        this.multiplexingGain = multiplexingGain;
    }

    @Override
    public String toString() {
        return "MultiplexingGainObject{" +
                "id='" + id + '\'' +
                ", multiplexingGain=" + multiplexingGain +
                '}';
    }
    @Override
    public int compareTo(MultiplexingGainObject o) {
        if (this.multiplexingGain > o.multiplexingGain)
            return -1;
        else if (o.multiplexingGain > this.multiplexingGain)
            return 1;
        return 0;
    }
}
