package cloudran;

public class TrafficObject implements Comparable<TrafficObject> {
    String id;
    double traffic;

    public TrafficObject(String id, double traffic) {
        this.id = id;
        this.traffic = traffic;
    }

    @Override
    public String toString() {
        return "TrafficObject{" +
                "id='" + id + '\'' +
                ", traffic=" + traffic +
                '}';
    }
    @Override
    public int compareTo(TrafficObject o) {
        if (this.traffic < o.traffic)
            return -1;
        else if (o.traffic < this.traffic)
            return 1;
        return 0;
    }
}
