package cloudran;

///////////////////////////////
//This class is the controller
///////////////////////////////

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.spec.RSAOtherPrimeInfo;
import java.util.*;

public class Network {
    //____________________________________________________________________________________________________________
    //____________________________________________________________________________________________________________
    // FUNCTION TO LOAD THE NETWORK (RRH/BBU) FROM A .csv FILE
    //____________________________________________________________________________________________________________
    //____________________________________________________________________________________________________________

    public List<Thing> cargar(String fileLoad) throws IOException {
        String archCSV1 = fileLoad + ".csv";
        FileReader fileReader = new FileReader(archCSV1);
        List<Thing> listThings = new ArrayList<>();
        StringBuilder attribute = new StringBuilder();
        int count = 1;
        int vlr=fileReader.read();
        while(vlr!=-1){
            attribute.setLength(0);
            Thing thing = new Thing();
            while(vlr!=10){
                attribute.append((char) vlr);
                if ((char) vlr == ';' && count==1){
                    thing.id = attribute.toString().replace(';',' ');
                    attribute.setLength(0);
                    count++;
                } else if((char) vlr == ';' && count==2){
                    thing.lat = Double.parseDouble(attribute.toString().replace(',','.').replace(';',' '));
                    attribute.setLength(0);
                    count++;
                }else if((char) vlr == ';' && count==3){
                    thing.lon = Double.parseDouble(attribute.toString().replace(',','.').replace(';',' '));
                    attribute.setLength(0);
                    count++;
                } else if( vlr == 13 && count==4){
                    thing.traffic = Double.parseDouble(attribute.toString().replace(',','.').replace(';',' '));
                    attribute.setLength(0);
                    listThings.add(thing);
                    count = 1;
                }
                vlr=fileReader.read();
            }
            vlr=fileReader.read();
        }
        //Cerramos el stream
        fileReader.close();
        return listThings;
    }


    //____________________________________________________________________________________________________________
    //____________________________________________________________________________________________________________
    // FUNCTIONS TO PERFORM THE NETWORK DISTRIBUTION
    //____________________________________________________________________________________________________________
    //____________________________________________________________________________________________________________


    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //  This function calculates the distance between two objects giving the lat and lon of each one   //
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    public double calculateDistance(Thing thing1, Thing thing2) {
        return calculateDistance(thing1.lat, thing1.lon, thing2.lat, thing2.lon);
    }

    public double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist);
    }

    // This function converts decimal degrees to radians
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    //  This function converts radians to decimal degrees
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    //////////////////////////////////////////////////////////////////////////////
    //                     NETWORK DISTRIBUTION By Delay                        //
    //////////////////////////////////////////////////////////////////////////////
    // Iteramos respecto a las RRH, conectándonos con las BBU que tengan mas cercana.
    // Como vemos esto será ineficiente, ya que no estamos balanceando en ningún momento.
    // Solamente tenemos que tener en cuenta que la distancia entre RRH-BBU debe ser menor a
    // 15km ya que a partir de 15km sobrepasaríamos el delay máximo permitido entre RRH-BBU.
    public void DistributeByDelay(List<Thing> RRHs, List<Thing> BBUs) {
        System.out.println("\n----------------------------------------");
        System.out.println("[Algorithm 1]: Distributing the Network by delay (distance)");
        System.out.println("----------------------------------------");


        // Creo un mapa con las BBUs para poder luego obtener el objeto al hacer get()
        HashMap<String, Thing> BBUsHashMap = new HashMap<>();
        for (Thing bbu : BBUs) {
            BBUsHashMap.put(bbu.id, bbu);
        }

        int i=0;
        // We connect each RRH with the closest BBU  **CONDITION: distance < 15 km) ***
        for (Thing rrh: RRHs){
            if(rrh.distanceList.get(0).distance < 15.0){
                String bbuToConnect = rrh.distanceList.get(i).id;
                //System.out.println("BBU to connect: " + bbuToConnect);
                Thing bbuToConnectObj = BBUsHashMap.get(bbuToConnect);
                rrh.connectedThings.add(bbuToConnectObj);
                bbuToConnectObj.traffic = bbuToConnectObj.traffic + rrh.traffic;
            }
            else{
                do{
                    i = i+1;
                }while(rrh.distanceList.get(i).distance < 15.0);
                String bbuToConnect = rrh.distanceList.get(i).id;
                System.out.println("BBU to connect: " + bbuToConnect);
                Thing bbuToConnectObj = BBUsHashMap.get(bbuToConnect);
                rrh.connectedThings.add(bbuToConnectObj);
                bbuToConnectObj.traffic = bbuToConnectObj.traffic + rrh.traffic;
                //TODO 1: Podriamos obligar que se diera este caso poniendo una RRH a tomar por culo de todas las BBU,
                // para al menos testear que lo hace bien
            }
        }


        // El problema es que tenemos para cada RRH, a que BBU está conectada,
        // pero quizás sería de utilidad saber para cada BBU, que RRH están conectadas
        // TODO 2: (Puede ser de gran utilidad)
        //  hacer una funcion que a partir de la lista connectedThings de las RRH me complete la lista connectedThings en las BBUs


        // TODO 3 (Opcional): Hacer que el programa devuelva un .csv ???
        // para poder ver de manera visual que está conectado
        // que por colores o algo así (?)
    }


    //////////////////////////////////////////////////////////////////////////////
    // Network Distribution by LOAD BALANCING OF THE NETWORK: Nº of RRHs   2.A) //
    //////////////////////////////////////////////////////////////////////////////
    public void BalanceByRRHConnectedAndDistance(List<Thing> RRHs, List<Thing> BBUs){
        System.out.println("\n----------------------------------------");
        System.out.println("[Algorithm 2.a]: Balancing the Network By Number of RRHs");
        System.out.println("----------------------------------------");

        // Creo un mapa con las RRH disponibles
        HashMap<String, Thing> AvailableRRHs = new HashMap<>();
        //HashMap<String, Thing> modifiedRRHs = new HashMap<>();
        for (Thing rrh : RRHs) {
            AvailableRRHs.put(rrh.id,rrh);
            //modifiedRRHs.put(rrh.id,rrh);
        }
        //A cada BBU se le llena su parámetro distanceList, con todas las RRH y la distancia hasta ellas. Luego se ordena.
        double distance;
        for (Thing bbu: BBUs){
            ArrayList<DistanceObject> distanceList = new ArrayList<DistanceObject>();
            for (Thing rrh: RRHs) {
                distance = calculateDistance(bbu,rrh);
                DistanceObject object = new DistanceObject(rrh.id, distance);
                distanceList.add(object);
            }
            bbu.distanceList = distanceList;
            Collections.sort(bbu.distanceList);
        }

        //Como ya tenemos ordenada la lista de RRH por distancia en cada BBU, ahora pasamos por todas las BBU y
        //le asignamos a su parametro connectedThings el primer objeto de la lista ordenada (distanceList)
        int contador;
        do {
            for(Thing bbu : BBUs){
                String rrhToConnect = bbu.distanceList.get(0).id;
                //System.out.println("RRH to connect: " + rrhToConnect);
                Thing rrhToConnectObj = AvailableRRHs.get(rrhToConnect);
                //Conecto la RRH a la BBU
                bbu.connectedThings.add(rrhToConnectObj);
                rrhToConnectObj.connectedThings.add(bbu);
                //Actualizo tráfico de la BBU
                bbu.traffic = bbu.traffic + rrhToConnectObj.traffic;
                //Borro del map AvailableRRHs la que acabo de asignar.
                AvailableRRHs.remove(rrhToConnect);
                //Y la borro también de la lista de distancias de todas las bbus, por medio de un ciclo.
                //Se hace como ves a continuacion para evitar ConcurrentModificationException
                for(Thing bbu1 : BBUs){
                    ArrayList<DistanceObject> toRemove = new ArrayList<>();
                    for(DistanceObject dO : bbu1.distanceList){
                        if(dO.id.equals(rrhToConnectObj.id)){
                            toRemove.add(dO);
                        }
                    }
                    bbu1.distanceList.removeAll(toRemove);
                }
                //System.out.println("Next RRH to connect: " + bbu.distanceList.get(0).id);
            }
            contador=BBUs.get(BBUs.size()-1).distanceList.size();
            //System.out.println("Size distanceList: " +contador);
        }while (contador>BBUs.size());

        for (int i=0; i< contador; i++){
            String rrhToConnect = BBUs.get(i).distanceList.get(0).id;
            Thing rrhToConnectObj = AvailableRRHs.get(rrhToConnect);
            BBUs.get(i).connectedThings.add(rrhToConnectObj);
            BBUs.get(i).traffic = BBUs.get(i).traffic + rrhToConnectObj.traffic;
            AvailableRRHs.remove(rrhToConnect);
            for(int j=0; j< contador; j++){
                ArrayList<DistanceObject> toRemove = new ArrayList<>();
                for(DistanceObject dO : BBUs.get(j).distanceList){
                    if(dO.id.equals(rrhToConnectObj.id)){
                        toRemove.add(dO);
                    }
                }
                BBUs.get(j).distanceList.removeAll(toRemove);
            }
        }

        for (Thing bbu : BBUs) {
            //System.out.println("BBU: " +bbu.id);
            //System.out.println("Total traffic: " +bbu.traffic);
            //System.out.println("RRHs availables: " +bbu.distanceList.size());
            for(DistanceObject dO : bbu.distanceList){
                //System.out.println("ID: " +dO.id + " at " + dO.distance);
            }
            //System.out.println("RRHs connected: ");
            for(int i=0; i<= bbu.connectedThings.size()-1; i++){
                //System.out.println(bbu.connectedThings.get(i).id);
            }
            //System.out.println("--------------------------------");
        }
    }


    //////////////////////////////////////////////////////////////////////////////
    // Network Distribution by LOAD BALANCING OF THE NETWORK: By traffic   2.b) //
    //////////////////////////////////////////////////////////////////////////////
    public void LoadBalanceByTraffic(List<Thing> RRHs, List<Thing> BBUs){
        System.out.println("\n----------------------------------------");
        System.out.println("[Algorithm 2.b]: Balancing the Network By Traffic");
        System.out.println("----------------------------------------");
        // Necesitamos una lista que lleve el control de el trafico de todas las BBUs
        ArrayList<TrafficObject> BBUsTrafficList = new ArrayList<TrafficObject>();

        // Creo un mapa con las RRH disponibles
        HashMap<String, Thing> AvailableRRHs = new HashMap<>();
        //HashMap<String, Thing> modifiedRRHs = new HashMap<>();

        for (Thing rrh : RRHs) {
            AvailableRRHs.put(rrh.id,rrh);
            //modifiedRRHs.put(rrh.id,rrh);
        }
        //A cada BBU se le llena su parámetro distanceList, con todas las RRH y la distancia hasta ellas. Luego se ordena.
        double distance;
        for (Thing bbu: BBUs){
            ArrayList<DistanceObject> distanceList = new ArrayList<DistanceObject>();
            for (Thing rrh: RRHs) {
                distance = calculateDistance(bbu,rrh);
                DistanceObject object = new DistanceObject(rrh.id, distance);
                distanceList.add(object);
            }
            bbu.distanceList = distanceList;
            Collections.sort(bbu.distanceList);
            TrafficObject bbuTrafficObject = new TrafficObject(bbu.id, bbu.traffic);
            BBUsTrafficList.add(bbuTrafficObject);
        }

        //Como ya tenemos ordenada la lista de RRH por distancia en cada BBU, ahora pasamos por todas las BBU y
        //evaluamos el primer objeto de la lista para conectar.
        int contador;
        int index = 0;
        do {
            //ORDENAMOS LA LISTA DE TRAFICO (EN CADA ITERACION)
            Collections.sort(BBUsTrafficList);

            //Encontrar la BBU con menor traffico y asignarle su RRH más cercana.
            String minorTrafic = BBUsTrafficList.get(index).id;
            for(Thing bbu:BBUs){
                if (bbu.id.equals(minorTrafic)){
                    String rrhToConnect = bbu.distanceList.get(0).id;
                    Thing rrhToConnectObj = AvailableRRHs.get(rrhToConnect);
                    if(calculateDistance(bbu,rrhToConnectObj)<=15){
                        //Conecto la RRH a la BBU; y viceversa.
                        bbu.connectedThings.add(rrhToConnectObj);
                        rrhToConnectObj.connectedThings.add(bbu);

                        //modifiedRRHs.replace(rrhToConnect, rrhToConnectObj);
                        //Actualizo tráfico de la BBU
                        bbu.traffic = bbu.traffic + rrhToConnectObj.traffic;

                        //Actualizo tráfico de la lista general de tráficos
                        BBUsTrafficList.get(0).traffic = bbu.traffic;

                        //Borro del map AvailableRRHs la que acabo de asignar.
                        AvailableRRHs.remove(rrhToConnect);

                        //Y la borro también de la lista de distancias de todas las bbus, por medio de un ciclo.
                        //Se hace como ves a continuacion para evitar ConcurrentModificationException
                        for(Thing bbu1 : BBUs){
                            ArrayList<DistanceObject> toRemove = new ArrayList<>();
                            for(DistanceObject dO : bbu1.distanceList){
                                if(dO.id.equals(rrhToConnectObj.id)){
                                    toRemove.add(dO);
                                }
                            }
                            bbu1.distanceList.removeAll(toRemove);
                        }
                        index = 0;
                    }else{
                        index++;
                    }
                }
            }
            contador=BBUs.get(BBUs.size()-1).distanceList.size();
        }while (contador>=BBUs.size());

        for (int i=0; i< contador; i++){
            Collections.sort(BBUsTrafficList);
            String rrhToConnect = BBUs.get(i).distanceList.get(0).id;
            Thing rrhToConnectObj = AvailableRRHs.get(rrhToConnect);
            BBUs.get(i).connectedThings.add(rrhToConnectObj);
            BBUs.get(i).traffic = BBUs.get(i).traffic + rrhToConnectObj.traffic;
            AvailableRRHs.remove(rrhToConnect);
            for(int j=0; j< contador; j++){
                ArrayList<DistanceObject> toRemove = new ArrayList<>();
                for(DistanceObject dO : BBUs.get(j).distanceList){
                    if(dO.id.equals(rrhToConnectObj.id)){
                        toRemove.add(dO);
                    }
                }
                BBUs.get(j).distanceList.removeAll(toRemove);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    //  This function DISTRIBUTE THE NETWORK BY MULTIPLEXING GAIN (Algorithm 3)  //
    ///////////////////////////////////////////////////////////////////////////////
    public void DistributeByMultiplexingGain(List<Thing> RRHs, List<Thing> BBUs){
        System.out.println("\n----------------------------------------");
        System.out.println("[Algorithm 3]: Balancing the network by Multiplexing Gain");
        System.out.println("----------------------------------------");

        // We need a list with all the possibly Multiplexing Gains that we would obtain if we add a RRH (The aim is select the BBU that maximize the M.G)
        ArrayList<MultiplexingGainObject> BBUsMultiplexingGainList = new ArrayList<MultiplexingGainObject>();

        // Creo un mapa con las BBUs para poder luego obtener el objeto al hacer get()
        HashMap<String, Thing> BBUsHashMap = new HashMap<>();
        for (Thing bbu : BBUs) {
            BBUsHashMap.put(bbu.id, bbu);
        }

        // Creo un mapa con las RRH disponibles
        HashMap<String, Thing> AvailableRRHs = new HashMap<>();
        for (Thing rrh : RRHs) {
            AvailableRRHs.put(rrh.id,rrh);
        }

        double multiplexGain;
        // Recorremos todas las RRHs. Las conectamos con la BBU que maximiza su M.G si esta RRH es incluida.
        for (Thing rrh: RRHs){
            // Para cada rrh, calculamos para cada BBU la M.G resultante si fuese añadida esta rrh.
            for(Thing bbu: BBUs){
                if(calculateDistance(bbu,rrh)<=15){
                    multiplexGain = ( (bbu.peakTraffic + rrh.peakTraffic) / (bbu.realTraffic + rrh.realTraffic ) );
                    bbu.multiplexingGainTemporal = multiplexGain; //La asigno como temporal para no dejar modificadas todas, ya que solo una será la BBU elejida.

                    // Añadimos a la lista de MultiplexGain cada una de las distintas combinaciones (para luego ordenar)
                    MultiplexingGainObject muxGainObject = new MultiplexingGainObject(bbu.id, bbu.multiplexingGainTemporal);
                    BBUsMultiplexingGainList.add(muxGainObject);
                }
            }
            //We sort the list, (remember that in this case we sort from the greater to the lower (check MultiplexingGainObject.java))
            Collections.sort(BBUsMultiplexingGainList);

            // Find the BBU that maximize its Multiplexing Gain if adding this rrh
            String maxMultiplexingGain = BBUsMultiplexingGainList.get(0).id;

            //We clear the MultiplexingGain List (We are going to generate one each iteration)
            BBUsMultiplexingGainList.clear();

            ////////////////////////////////////////////////////////////////////////////////////////
            // TODO 1: EL ALGORITMO LAS PRIMERAS ITERACIONES VA SER DEMASIADO INEFICIENTE NO?
            //         QUE TAL SI ASIGNO A cada BBU sus 5 torres mas cercana y a partir de aqui lo lanzo?
            //
            // TODO 2: Otra cosa que hay que hacer es implementar algo de aleteoriedad en caso de que todas
            //         las BBUs tengan la misma MG en una iteración
            //
            // TODO 3: Desordenar el archivo de RRHs
            ////////////////////////////////////////////////////////////////////////////////////////

            // Connect to it
            //String rrhToConnect = bbu.distanceList.get(0).id;
            Thing bbuToConnectObj = BBUsHashMap.get(maxMultiplexingGain);

            // We connect it ( bidirectional (?) second line from Dani.. check* )
            bbuToConnectObj.connectedThings.add(rrh);
            rrh.connectedThings.add(bbuToConnectObj);

            // We update the Multiplex Gain of this BBU. (Atencion! ahora es bbu.multiplexinGain, no bbu.multiplexinGainTemporal !!)
            multiplexGain = ( (bbuToConnectObj.peakTraffic + rrh.peakTraffic) / (bbuToConnectObj.realTraffic + rrh.realTraffic ) );
            bbuToConnectObj.multiplexingGain = multiplexGain;

            //We update peakTraffic and realTraffic
            bbuToConnectObj.peakTraffic = (bbuToConnectObj.peakTraffic + rrh.peakTraffic);
            bbuToConnectObj.realTraffic = (bbuToConnectObj.realTraffic + rrh.realTraffic);

            //We update the traffic (we don't need it in the algorithm, is just to have the traffic to include it in the report)
            bbuToConnectObj.traffic = bbuToConnectObj.traffic + rrh.traffic;

            // Once it is connected, we have to delete this RRH from the Available's RRH list.
            AvailableRRHs.remove(rrh);
        }
    }
}