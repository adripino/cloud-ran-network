package cloudran;

// Esta clase representa las RRH.
// Contiene la posición de cada torre (lat y lon), y el id.
// Contiene una lista con las distancias a cada una de las BBU, y contiene un campo
// que nos indica a que BBU está conectada.

import java.util.ArrayList;
import java.util.List;

public class RRH extends Thing{
    boolean isConnected;

    ArrayList<DistanceObject> DistBBUs;

    //Constructor
    public RRH(double lat, double lon, String id){
        this.lat = lat;
        this.lon = lon;
        this.id = id;
    }

}
