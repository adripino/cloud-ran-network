package cloudran;

public class DistanceObject implements Comparable<DistanceObject>{
    String id;
    double distance;

    public DistanceObject(String id, double distance) {
        this.id = id;
        this.distance = distance;
    }

    @Override
    public int compareTo(DistanceObject o) {
        if(this.distance<o.distance)
            return -1;
        else if(o.distance<this.distance)
            return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "DistanceObject{" +
                "id='" + id + '\'' +
                ", distance=" + distance +
                '}';
    }
}
