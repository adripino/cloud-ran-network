package cloudran;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Thing {
    String id;
    double lat;
    double lon;

    // Variables used in Traffic Algorithm's (Load Balancing by traffic):
    double traffic; //For the RRH is gonna be a fixed value, and for the BBU is gonna change dynamically.

    // Variables used in Multiplexing Gain's algorithm
    double peakTraffic;
    double realTraffic;
    double multiplexingGain;

    double peakTrafficTemporal;
    double realTrafficTemporal;
    double multiplexingGainTemporal;


    ArrayList<DistanceObject> distanceList;
    ArrayList<Thing> connectedThings;
    ArrayList<MultiplexingGainObject> MultiplexingList;

    //Constructor
    public Thing(){
        this.connectedThings = new ArrayList<>();
    }
}