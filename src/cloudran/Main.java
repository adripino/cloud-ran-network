package cloudran;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLOutput;
import java.util.*;
import static javafx.scene.input.KeyCode.R;

//TODO 1: Implementar una lógica para poder elegir que algoritmo aplicar sobre los input files. (Un switch)
//TODO 2: Hacer funcion para printar información sobre la red; distribución por distancias, Trafico de cada BBU, etc.

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("***************************");
        System.out.println("  Welcome to the program");
        System.out.println("***************************");
        // We declare the controller. In this case is called "nx"
        Network nx = new Network();

        // Scanner
        Scanner sn = new Scanner(System.in);

        // We save the default print mode (console)
        PrintStream console = System.out;

        //////////////////////////////////////
        // We load the BBUs into a list
        //////////////////////////////////////
        List<Thing> BBUs = nx.cargar("bbupools");

        System.out.println("[*] BBUs loaded. Number of BBUs: " + BBUs.size());

        // Imprimimos las BBU
        //for (int m = 0; m <= BBUs.size() - 1; m++) {
        //    System.out.println("ID: " + BBUs.get(m).id + " Lat: "
        //            + BBUs.get(m).lat + " Lon: " + BBUs.get(m).lon
        //    );
        //}

        //////////////////////////////////////
        // We load the RRhs into another list
        //////////////////////////////////////
        List<Thing> RRHs = nx.cargar("cells");
        System.out.println("[*] RHHs loaded. Number of sites: " + RRHs.size());


        //////////////////////////////////////
        // We calculate the DISTANCES from each RRH to all
        // the BBUs and we save it in an ArrayList<String>
        // (And we sort the list of distances)
        //////////////////////////////////////
        double distance;
        for (Thing rrh : RRHs){
            ArrayList<DistanceObject> DistanceList = new ArrayList<DistanceObject>();
            for (Thing bbu : BBUs) {
                distance = nx.calculateDistance(bbu, rrh);
                DistanceObject object = new DistanceObject(bbu.id, distance);
                DistanceList.add(object);
            }
            rrh.distanceList = DistanceList;
            Collections.sort(rrh.distanceList);

            // Aprovecho el ciclo para también hacer la asignación de trafico a cada RRH
            // [Edit] Aprovechamos para asignar las variables necesarias para calcular la multiplexacion por ganancia
            // (MultiplexingGain = (PeakTraffic / RealTraffic (at 21.00h) )
            if(rrh.traffic == 1){       // 1 == Residential
                rrh.traffic = 1289.33;
                rrh.peakTraffic = 1641.6309;
                rrh.realTraffic = 1607.0403;
            }
            if(rrh.traffic == 2){       // 2 == Office
                rrh.traffic = 1163.86;
                rrh.peakTraffic = 1216.6698;
                rrh.realTraffic = 257.7382;
            }
            if(rrh.traffic == 3){       // 3 == Mixed
                rrh.traffic = 2715.97;
                rrh.peakTraffic = 2715.97;
                rrh.realTraffic= 2075.4796;
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Distribution By Delay (Algorithm 1)
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        nx.DistributeByDelay(RRHs, BBUs);

        // We generate a NetworkDistributedByDelay.txt
        PrintStream fileStream = new PrintStream("NetworkDistributedByDelay.txt");
        System.setOut(fileStream);
        for (Thing rrh2: RRHs) {
            System.out.println(" RRH: "+rrh2.id+ "Conectada con BBU: "+rrh2.connectedThings.get(0).id);
        }

        // We set again the default sout mode
        System.setOut(console);
        System.out.println("   [NetworkDistributedByDelay.txt generated]");

        System.out.println("\n-------------------------------------");
        System.out.println("Traffic per BBU Applying Algorithm 1");
        System.out.println("-------------------------------------");
        for (Thing bbu : BBUs) {
            System.out.print("BBU: " +bbu.id);
            System.out.println("  -  Total traffic: " +bbu.traffic);
        }

        // Funcion que nos diga cuantas RRH hay por rango de distancias (que nos permita ver como está distribuida nuestra red en función de la distancia (fibra))
        double distanceDistribution;
        int cont1 = 0,cont2 = 0,cont3 = 0,cont4 = 0,cont5 = 0,cont6 = 0,cont7 = 0;
        for (Thing rrh : RRHs) {
            Thing BBU = rrh.connectedThings.get(0);
            distanceDistribution = nx.calculateDistance(BBU, rrh);
            if(distanceDistribution < 1){
                cont1++;
            }
            else if((1 <= distanceDistribution) && (distanceDistribution < 2)){
                cont2++;
            }
            else if((2 <= distanceDistribution) && (distanceDistribution < 3)){
                cont3++;
            }
            else if((3 <= distanceDistribution) && (distanceDistribution < 4)){
                cont4++;
            }
            else if((4 <= distanceDistribution) && (distanceDistribution < 5)){
                cont5++;
            }
            else if((5 <= distanceDistribution) && (distanceDistribution < 6)){
                cont6++;
            }
            else if((6 <= distanceDistribution) && (distanceDistribution < 7)){
                cont7++;
            }
            else{
                System.out.println("MAYOR QUE 7 km");
            }
        }
        */

        /*System.out.println("Distribucion de RRH según distancias (0-1km, 1-2km...)");
        System.out.println(cont1);
        System.out.println(cont2);
        System.out.println(cont3);
        System.out.println(cont4);
        System.out.println(cont5);
        System.out.println(cont6);
        System.out.println(cont7);
        */

        //////////////////////////////////////
        // LOAD BALANCING BY RRHs CONNECTED (Algorithm 2.a)
        //////////////////////////////////////

        /*nx.BalanceByRRHConnectedAndDistance(RRHs, BBUs);

        // We generate a NetworkDistributedByRRHConnectedAndDistance.txt
        PrintStream fileStream2 = new PrintStream("NetworkDistributedByRRHConnectedAndDistance.txt");
        System.setOut(fileStream2);
        int i;
        for (Thing bbu2: BBUs) {
            System.out.println("\nBBU: "+bbu2.id+ "Conectada con las siguientes RRH: ");
            for(i = 0; i <= (bbu2.connectedThings.size() - 1); i++){
                System.out.print(bbu2.connectedThings.get(i).id+ "\n");
            }
        }

        // We set again the default sout mode
        System.setOut(console);
        System.out.println("   [NetworkDistributedByRRHConnectedAndDistance.txt generated]");

        System.out.println("\n-------------------------------------");
        System.out.println("Traffic per BBU Applying Algorithm 2.a");
        System.out.println("-------------------------------------");
        for (Thing bbu : BBUs) {
            System.out.print("BBU: " +bbu.id);
            System.out.println("  -  Total traffic: " +bbu.traffic);
        }*/

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // LOAD BALANCING BY Traffic (Algorithm 2.b)
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Lista del trafico de cada BBU


        nx.LoadBalanceByTraffic(RRHs, BBUs);

        // We generate a NetworkDistributedByRRHConnectedAndDistance.txt
        PrintStream fileStream3 = new PrintStream("NetworkDistributedByTraffic.txt");
        System.setOut(fileStream3);
        int i;
        for (Thing bbu2: BBUs) {
            System.out.println("\nBBU: "+bbu2.id+ "Conectada con las siguientes RRH: ");
            for(i = 0; i <= (bbu2.connectedThings.size() - 1); i++){
                System.out.print(bbu2.connectedThings.get(i).id+ "\n");
            }
        }

        // We set again the default sout mode
        System.setOut(console);
        System.out.println("   [NetworkDistributedByTraffic.txt generated]");


        System.out.println("\n-------------------------------------");
        System.out.println("Traffic per BBU Applying Algorithm 2.b");
        System.out.println("-------------------------------------");
        for (Thing bbu : BBUs) {
            System.out.print("BBU: " +bbu.id);
            System.out.print("  -  Total traffic: " +bbu.traffic);
            System.out.println("  -  Total RRHs: " +bbu.connectedThings.size());
        }

        // Funcion que nos diga cuantas RRH hay por rango de distancias (que nos permita ver como está distribuida nuestra red en función de la distancia (fibra))
        double distanceDistribution = 0;
        int cont1 = 0,cont2 = 0,cont3 = 0,cont4 = 0,cont5 = 0,cont6 = 0,cont7 = 0,cont8 = 0;
        for (Thing bbu : BBUs) {
            for(Thing RRH : bbu.connectedThings) {
                distanceDistribution = nx.calculateDistance(bbu, RRH);
                if (distanceDistribution < 1) {
                    cont1++;
                } else if ((1 <= distanceDistribution) && (distanceDistribution < 2)) {
                    cont2++;
                } else if ((2 <= distanceDistribution) && (distanceDistribution < 3)) {
                    cont3++;
                } else if ((3 <= distanceDistribution) && (distanceDistribution < 4)) {
                    cont4++;
                } else if ((4 <= distanceDistribution) && (distanceDistribution < 5)) {
                    cont5++;
                } else if ((5 <= distanceDistribution) && (distanceDistribution < 6)) {
                    cont6++;
                } else if ((6 <= distanceDistribution) && (distanceDistribution < 7)) {
                    cont7++;
                } else {
                    //System.out.println("MAYOR QUE 7 km");
                    cont8++;
                }
            }
        }
        System.out.println("\n-------------------------------------");
        System.out.println("Distribucion de RRH según distancias (0-1km, 1-2km...)");
        System.out.println("-------------------------------------");
        System.out.println("RRHs conectadas con BBUs a:");
        System.out.println("< 1km: "+cont1);
        System.out.println("1-2km: "+cont2);
        System.out.println("2-3km: "+cont3);
        System.out.println("3-4km: "+cont4);
        System.out.println("4-5km: "+cont5);
        System.out.println("5-6km: "+cont6);
        System.out.println("6-7km: "+cont7);
        System.out.println("> 7km: "+cont8);


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Distribute by Multiplexing Gain
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*nx.DistributeByMultiplexingGain(RRHs, BBUs);

        // We generate a NetworkDistributedByMultiplexingGain.txt
        PrintStream fileStream4 = new PrintStream("NetworkDistributedByMultiplexingGain.txt");
        System.setOut(fileStream4);
        for (Thing rrh2: RRHs) {
            System.out.println(" RRH: "+rrh2.id+ "Conectada con BBU: "+rrh2.connectedThings.get(0).id);
        }

        // We set again the default sout mode
        System.setOut(console);
        System.out.println("   [NetworkDistributedByMultiplexingGain.txt generated]");


        System.out.println("\n-------------------------------------");
        System.out.println("Traffic per BBU Applying Algorithm 3");
        System.out.println("-------------------------------------");
        for (Thing bbu : BBUs) {
            System.out.print("BBU: " +bbu.id);
            System.out.println("  -  Total traffic: " +bbu.traffic);
        }

        // Funcion que nos diga cuantas RRH hay por rango de distancias (que nos permita ver como está distribuida nuestra red en función de la distancia (fibra))
        double distanceDistribution;
        int cont1 = 0,cont2 = 0,cont3 = 0,cont4 = 0,cont5 = 0,cont6 = 0,cont7 = 0,cont8 = 0;
        for (Thing rrh : RRHs) {
            Thing BBU = rrh.connectedThings.get(0);
            distanceDistribution = nx.calculateDistance(BBU, rrh);
            if(distanceDistribution < 1){
                cont1++;
            }
            else if((1 <= distanceDistribution) && (distanceDistribution < 2)){
                cont2++;
            }
            else if((2 <= distanceDistribution) && (distanceDistribution < 3)){
                cont3++;
            }
            else if((3 <= distanceDistribution) && (distanceDistribution < 4)){
                cont4++;
            }
            else if((4 <= distanceDistribution) && (distanceDistribution < 5)){
                cont5++;
            }
            else if((5 <= distanceDistribution) && (distanceDistribution < 6)){
                cont6++;
            }
            else if((6 <= distanceDistribution) && (distanceDistribution < 7)){
                cont7++;
            }
            else{
                //System.out.println("MAYOR QUE 7 km");
                cont8++;
            }
        }
        System.out.println("\n-------------------------------------");
        System.out.println("Distribucion de RRH según distancias (0-1km, 1-2km...)");
        System.out.println("-------------------------------------");
        System.out.println("Menor de 1: "+cont1);
        System.out.println(cont2);
        System.out.println(cont3);
        System.out.println(cont4);
        System.out.println(cont5);
        System.out.println(cont6);
        System.out.println(cont7);
        System.out.println("Mayor que 7: "+cont8);


        System.out.println("\n-------------------------------------");
        System.out.println("Multiplexing Gain per BBU Applying Algorithm 3");
        System.out.println("-------------------------------------");
        for (Thing bbu : BBUs) {
            System.out.print("BBU: " +bbu.id);
            System.out.println("  -  Total traffic: " +bbu.multiplexingGain);
        }*/
    }
}
